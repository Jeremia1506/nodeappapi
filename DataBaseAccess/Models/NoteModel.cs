﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseAccess.Models
{
    public class NoteModel
    {
        public int NoteID { get; set; } = 0;
        public int UserID { get; set; } = 0;
        public string NoteText { get; set; } = "";
        public string Header { get; set; } = "";
    }
}
