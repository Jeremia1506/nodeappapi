﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseAccess.Models
{
    public class UserModel
    {
        public int UserID { get; set; } = 0;
        public string UserName { get; set; } = "";
        public string UserPassword { get; set; } = "";
        public string JwtToken { get; set; }
    }
}
