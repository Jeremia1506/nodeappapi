﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess
{
    interface IAccessDb<ReturnTyp, ParaTyp>
    {

        Task<object> DoAccessDb(ParaTyp para, DoAccessDb doAccessDb);

        Task<ReturnTyp> DoConvertDbValues(object dbValues);
    }
}
