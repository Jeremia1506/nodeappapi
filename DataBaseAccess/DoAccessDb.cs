﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DataBaseAccess
{
    class DoAccessDb
    {
        private MySqlConnection DbConnection = null;
        private string _ConnectionString = @"server=192.168.178.56;user id = root; password=9364;port=3306;database=API;";
        public bool OpenConnection()
        {
            try
            {
                DbConnection = new MySqlConnection(_ConnectionString);
                DbConnection.Open();
                Console.WriteLine("Datenbank verbindung wurde hergestellt.");
                return true;

            }
            catch(MySqlException e)
            {
                Console.WriteLine("Datenbank verbindung konnte nicht hergestellt werden! " + e.Message);
                return false;
            }
        }

        public DataTable GetDb(string sql, MySqlParameter[] mySqlParameter)
        {
            var dt = new DataTable();
            try
            {
                using (var cmd = DbConnection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    foreach (var item in mySqlParameter)
                    {
                        cmd.Parameters.Add(item);
                    }

                    using (var reader = cmd.ExecuteReader())
                    {
                        var ds = new DataSet();

                        ds.Tables.Add(dt);
                        ds.EnforceConstraints = false;
                        dt.Load(reader);
                        reader.Close();
                    }
                    cmd.Dispose();
                }
            }
            catch(MySqlException e)
            {
                Console.WriteLine($"Fehler im Sql! {sql} : {e.Message}");
                CloseConnection();
            }
            return dt;
        }

        public async Task<bool> ExecDb(string sql, MySqlParameter[] mySqlParameter)
        {
            bool result = false;
            try
            {
                using (var cmd = DbConnection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    foreach (var item in mySqlParameter)
                    {
                        cmd.Parameters.Add(item);
                    }
                    await cmd.ExecuteNonQueryAsync();
                    result = true;
                }
            }
            catch (MySqlException e)
            {
                Console.WriteLine($"Fehler im Sql! {sql} : {e.Message}");
                CloseConnection();
            }
            return result;
        }

        public bool CloseConnection()
        {
            try
            {
                DbConnection.Dispose();
                Console.WriteLine("Connection zur Datenbank wurde geschlossen.");
                return true;
            }
            catch
            {
                Console.WriteLine("Connection zur Datenbank konnte nicht geschlossen werden!");
                return false;
            }
        }
    }
}
