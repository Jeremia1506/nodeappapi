﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBaseAccess
{
    public enum eAccessDb
    {
        InsertUser = 0,
        ReadUser = 1,
        DeleteNote = 2,
        UpdateNote = 3,
        InsertNote = 4,
        ReadNote = 5
    }
}
