﻿using DataBaseAccess.Access;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess
{
    public class ControllDataBaseAccess
    {
        private DoAccessDb _DoAccessDb;

        public ControllDataBaseAccess()
        {
            _DoAccessDb = new DoAccessDb();
            _DoAccessDb.OpenConnection();
        }

        public async Task<bool> ExecDbAsync<ParaTyp>(ParaTyp para, eAccessDb accessDb) 
        {
            var access = _GetIAccessDb<bool, ParaTyp>(accessDb);
            return await access.DoConvertDbValues(await access.DoAccessDb(para, _DoAccessDb));
        }
        public bool ExecDb<ParaTyp>(ParaTyp para, eAccessDb accessDb)
        {
            var access = _GetIAccessDb<bool, ParaTyp>(accessDb);
            var getData = access.DoAccessDb(para, _DoAccessDb);
            getData.Wait();
            var convertData = access.DoConvertDbValues(getData.Result);
            convertData.Wait();
            return convertData.Result;
        }

        public async Task<ReturnTyp> GetDbAsync<ReturnTyp, ParaTyp>(ParaTyp para, eAccessDb accessDb)
        {
            var access = _GetIAccessDb<ReturnTyp, ParaTyp>(accessDb);
            return await access.DoConvertDbValues(await access.DoAccessDb(para, _DoAccessDb));
        }

        public ReturnTyp GetDb<ReturnTyp, ParaTyp>(ParaTyp para, eAccessDb accessDb)
        {
            var access = _GetIAccessDb<ReturnTyp, ParaTyp>(accessDb);
            var getData = access.DoAccessDb(para, _DoAccessDb);
            getData.Wait();
            var convertData = access.DoConvertDbValues(getData.Result);
            convertData.Wait();
            return convertData.Result;

        }

        private IAccessDb<ReturnTyp, ParaTyp> _GetIAccessDb<ReturnTyp, ParaTyp>(eAccessDb accessDb)
        {
            switch (accessDb)
            {
                case eAccessDb.InsertUser:
                    return (IAccessDb<ReturnTyp, ParaTyp>)new InsertUser();
                case eAccessDb.ReadUser:
                    return (IAccessDb<ReturnTyp, ParaTyp>)new ReadUser();
                case eAccessDb.DeleteNote:
                    return (IAccessDb<ReturnTyp, ParaTyp>)new DeleteNote();
                case eAccessDb.InsertNote:
                    return (IAccessDb<ReturnTyp, ParaTyp>)new InsertNote();
                case eAccessDb.UpdateNote:
                    return (IAccessDb<ReturnTyp, ParaTyp>)new UpdateNote();
                case eAccessDb.ReadNote:
                    return (IAccessDb<ReturnTyp, ParaTyp>)new ReadNote();
                default:
                    return null;
            }
        }
    }
}
