﻿using DataBaseAccess.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess.Access
{
    class ReadNote : IAccessDb<List<NoteModel>, NoteModel>
    {
        public Task<object> DoAccessDb(NoteModel para, DoAccessDb doAccessDb)
        {
            object result = null;
            try
            {
                string sql = "Select * From Notes";
                string sqlPara = "";

                var sqlParaList = new MySqlParameter[4]
                {
                    new MySqlParameter("@param_Header", $"%{para.Header}%"),
                    new MySqlParameter("@param_NoteID", para.NoteID),
                    new MySqlParameter("@param_NoteText", $"%{para.NoteText}%"),
                    new MySqlParameter("@param_UserID", para.UserID)
                };

                if(para.Header != "")
                    sqlPara += " AND Header Like @param_Header";
                if (para.NoteID != 0)
                    sqlPara += " AND NoteID = @param_NoteID";
                if (para.NoteText != "")
                    sqlPara += " AND NoteText Like @param_NoteText";
                if (para.UserID != 0)
                    sqlPara += " AND UserID = @param_UserID";
                if (sqlPara != "")
                    sql += $" WHERE{sqlPara.Remove(0, 4)}";

                result = doAccessDb.GetDb(sql, sqlParaList);
            }
            catch
            {

            }
            return Task.FromResult(result);
        }

        public Task<List<NoteModel>> DoConvertDbValues(object dbValues)
        {
            var result = new List<NoteModel>();

            try
            {
                foreach (DataRow row in ((DataTable)dbValues).Rows)
                {
                    result.Add(new NoteModel()
                    {
                        Header = row["Header"].ToString(),
                        NoteText = row["NoteText"].ToString(),
                        NoteID = Convert.ToInt32(row["NoteID"]),
                        UserID = Convert.ToInt32(row["UserID"])
                    });
                }
            }
            catch
            {

            }

            return Task.FromResult(result);
        }
    }
}
