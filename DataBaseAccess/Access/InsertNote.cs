﻿using DataBaseAccess.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess.Access
{
    class InsertNote : IAccessDb<bool, NoteModel>
    {
        public async Task<object> DoAccessDb(NoteModel para, DoAccessDb doAccessDb)
        {
            object result = null;
            try
            {
                string sql = @"Insert into Notes (UserID, NoteText, Header) Values (@param_UserID, @param_NoteText, @param_Header)";
                var parameters = new MySqlParameter[3]
                {
                    new MySqlParameter("@param_UserID", para.UserID),
                    new MySqlParameter("@param_NoteText", para.NoteText),
                    new MySqlParameter("@param_Header", para.Header)
                };
                await doAccessDb.ExecDb(sql, parameters);
                result = new object();
            }
            catch
            {

            }
            return Task.FromResult(result);
        }

        public Task<bool> DoConvertDbValues(object dbValues)
        {
            return Task.FromResult((dbValues == null) ? false : true);
        }
    }
}
