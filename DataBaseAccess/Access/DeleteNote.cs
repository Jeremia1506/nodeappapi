﻿using DataBaseAccess.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess.Access
{
    class DeleteNote : IAccessDb<bool, NoteModel>
    {
        public async Task<object> DoAccessDb(NoteModel para, DoAccessDb doAccessDb)
        {
            object result = null;

            try
            {
                string sql = "Delete From Notes Where NoteID = @param_NoteID";
                var parameter = new MySqlParameter[1]
                {
                    new MySqlParameter("@param_NoteID", para.NoteID)
                };
                await doAccessDb.ExecDb(sql, parameter);
                result = new object();
            }
            catch
            {

            }

            return result;
        }

        public Task<bool> DoConvertDbValues(object dbValues)
        {
            return Task.FromResult((dbValues == null) ? false : true);
        }
    }
}
