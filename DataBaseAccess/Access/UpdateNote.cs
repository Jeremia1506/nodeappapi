﻿using DataBaseAccess.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess.Access
{
    class UpdateNote : IAccessDb<bool, NoteModel>
    {
        public async Task<object> DoAccessDb(NoteModel para, DoAccessDb doAccessDb)
        {
            object result = null;

            try
            {
                string sql = "Update Notes Set NoteText = @param_NoteText, Header = @param_Header WHERE NoteID = @param_NoteID";
                var parameters = new MySqlParameter[3]
                {
                    new MySqlParameter("@param_NoteText", para.NoteText),
                    new MySqlParameter("@param_Header", para.Header),
                    new MySqlParameter("@param_NoteID", para.NoteID)
                };
                await doAccessDb.ExecDb(sql, parameters);
                result = new object();
            }
            catch
            {

            }

            return result;
        }

        public Task<bool> DoConvertDbValues(object dbValues)
        {
            return Task.FromResult((dbValues == null) ? false : true);
        }
    }
}
