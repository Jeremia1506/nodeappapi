﻿using DataBaseAccess.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess.Access
{
    class InsertUser : IAccessDb<bool, UserModel>
    {
        public async Task<object> DoAccessDb(UserModel para, DoAccessDb doAccessDb)
        {
            object result = null;
            try
            {
                string sql = @"Insert into User (Username, UserPassword) Values (@param_UserName, @param_UserPassword)";
                var parameters = new MySqlParameter[2]
                {
                    new MySqlParameter("@param_UserName", para.UserName),
                    new MySqlParameter("@param_UserPassword", para.UserPassword)
                };
                await doAccessDb.ExecDb(sql, parameters);
                result = new object();
            }
            catch
            {

            }
            return result;
        }

        public Task<bool> DoConvertDbValues(object dbValues)
        {
            return Task.FromResult((dbValues == null) ? false : true);
        }
    }
}
