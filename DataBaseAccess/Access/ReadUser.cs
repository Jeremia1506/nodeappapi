﻿using DataBaseAccess.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseAccess.Access
{
    class ReadUser : IAccessDb<List<UserModel>, UserModel>
    {
        public Task<object> DoAccessDb(UserModel para, DoAccessDb doAccessDb)
        {
            object result = null;
            try
            {
                string sql = "Select * From User";
                string sqlPara = "";
                var sqlParaList = new MySqlParameter[3]
                {
                    new MySqlParameter("@param_UserID", para.UserID),
                    new MySqlParameter("@param_UserName", para.UserName),
                    new MySqlParameter("@param_UserPassword", para.UserPassword)
                };
                if (para.UserID != 0)
                    sqlPara += " AND UserID = @param_UserID";
                if (para.UserName != "")
                    sqlPara += " AND UserName = @param_UserName";
                if (para.UserPassword != "")
                    sqlPara += " AND UserPassword = @param_UserPassword";
                if (sqlPara != "")
                    sql += $" WHERE{sqlPara.Remove(0,4)}";

                result = doAccessDb.GetDb(sql, sqlParaList);
            }
            catch
            {

            }
            return Task.FromResult(result);
        }

        public Task<List<UserModel>> DoConvertDbValues(object dbValues)
        {
            var result = new List<UserModel>();
            try
            {
                foreach (DataRow row in ((DataTable)dbValues).Rows)
                {
                    result.Add(new UserModel()
                    {
                        UserID = Convert.ToInt32(row["UserID"]),
                        UserName = row["UserName"].ToString(),
                        UserPassword = row["UserPassword"].ToString()
                    });
                }
            }
            catch
            {

            }
            return Task.FromResult(result);
        }
    }
}
