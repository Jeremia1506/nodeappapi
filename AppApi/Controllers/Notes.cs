﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataBaseAccess;
using DataBaseAccess.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Notes : ControllerBase
    {

        private ControllDataBaseAccess _ControllDataBaseAccess = new ControllDataBaseAccess();

        // GET: api/<Notes>
        [Authorize]
        [HttpGet]
        public List<NoteModel> Get()
        {
            return _ControllDataBaseAccess.GetDb<List<NoteModel>, NoteModel>(
                new NoteModel(),
                eAccessDb.ReadNote
                );
        }

        [Authorize]
        [HttpGet("{NoteID}")]
        public List<NoteModel> Get(int NoteID)
        {
            return _ControllDataBaseAccess.GetDb<List<NoteModel>, NoteModel>(
                new NoteModel() 
                { 
                    NoteID = NoteID
                },
                eAccessDb.ReadNote
                );
        }

        [Authorize]
        [HttpGet("{NoteID}/{UserID}")]
        public List<NoteModel> Get(int NoteID, int UserID)
        {
            return _ControllDataBaseAccess.GetDb<List<NoteModel>, NoteModel>(
                new NoteModel()
                {
                    NoteID = NoteID,
                    UserID = UserID
                },
                eAccessDb.ReadNote
                );
        }

        [Authorize]
        [HttpGet("{NoteID}/{UserID}/{Header}")]
        public List<NoteModel> Get(int NoteID, int UserID, string Header)
        {
            return _ControllDataBaseAccess.GetDb<List<NoteModel>, NoteModel>(
                new NoteModel()
                {
                    NoteID = NoteID,
                    UserID = UserID,
                    Header = Header
                },
                eAccessDb.ReadNote
                );
        }

        [Authorize]
        [HttpGet("{NoteID}/{UserID}/{Header}/{NoteText}")]
        public List<NoteModel> Get(int NoteID, int UserID, string Header, string NoteText)
        {
            return _ControllDataBaseAccess.GetDb<List<NoteModel>, NoteModel>(
                new NoteModel()
                {
                    NoteID = NoteID,
                    UserID = UserID,
                    Header = Header,
                    NoteText = NoteText
                },
                eAccessDb.ReadNote
                );
        }

        // POST api/<Notes>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<NoteModel>> Post([FromBody] NoteModel note)
        {
            if(await _ControllDataBaseAccess.ExecDbAsync<NoteModel>(note, eAccessDb.InsertNote))
            {
                var newDbNode = await _ControllDataBaseAccess.GetDbAsync<List<NoteModel>, NoteModel>(note, eAccessDb.ReadNote);
                if(newDbNode.Count == 1)
                {
                    return CreatedAtAction(
                        nameof(Get),
                        new
                        {
                            NoteID = note.NoteID,
                            Header = note.Header,
                            NoteText = note.NoteText
                        },
                        newDbNode
                        );
                }
            }
            return CreatedAtAction(nameof(Get), new { }, note);
        }

        // PUT api/<Notes>/5
        [Authorize]
        [HttpPut("{NoteID}")]
        public async Task<ActionResult<NoteModel>> Put(int NoteID, [FromBody] NoteModel note)
        {
            note.NoteID = NoteID;
            if (await _ControllDataBaseAccess.ExecDbAsync<NoteModel>(note, eAccessDb.UpdateNote))
            {
                var newDbNode = await _ControllDataBaseAccess.GetDbAsync<List<NoteModel>, NoteModel>(note, eAccessDb.ReadNote);
                if (newDbNode.Count == 1)
                {
                    return CreatedAtAction(
                        nameof(Get),
                        new
                        {
                            NoteID = note.NoteID,
                            Header = note.Header,
                            NoteText = note.NoteText
                        },
                        newDbNode
                        );
                }
            }
            return CreatedAtAction(nameof(Get), new { }, note);
        }

        // DELETE api/<Notes>/5
        [Authorize]
        [HttpDelete("{NoteID}")]
        public async Task<ActionResult<NoteModel>> Delete(int NoteID)
        {
            var note = new NoteModel()
            {
                NoteID = NoteID
            };
            if (await _ControllDataBaseAccess.ExecDbAsync<NoteModel>(note, eAccessDb.DeleteNote))
            {
                var newDbNode = await _ControllDataBaseAccess.GetDbAsync<List<NoteModel>, NoteModel>(note, eAccessDb.ReadNote);
                if (newDbNode.Count == 0)
                {
                    return CreatedAtAction(
                        nameof(Get),
                        new
                        {
                            NoteID = NoteID
                        },
                        new NoteModel() { NoteID = NoteID}
                        );
                }
            }
            return CreatedAtAction(nameof(Get), new { }, null);
        }
    }
}
