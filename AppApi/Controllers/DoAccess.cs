﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using AppApi.Logic;
using DataBaseAccess;
using DataBaseAccess.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AppApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DoAccess : ControllerBase
    {
        private ControllDataBaseAccess _ControllDataBaseAccess = null;
        private Jwt _Jwt = null;

        public DoAccess(IConfiguration config)
        {
            _ControllDataBaseAccess = new ControllDataBaseAccess();
            _Jwt = new Jwt(config);
        }


        // GET: api/<Access>
        [HttpGet("{Username}/{UserPassword}")]
        public async Task<UserModel> Get(string Username, string UserPassword)
        {
            var user = new UserModel()
            {
                UserName = Username,
                UserPassword = UserPassword
            };
            var dbUser = await _ControllDataBaseAccess.GetDbAsync<List<UserModel>, UserModel>(user, eAccessDb.ReadUser);
            if (dbUser.Count == 1)
            {
                string tokenStr = _Jwt.GenerateJSONWebToken(dbUser[0]);

                return new UserModel()
                {
                    UserName = dbUser[0].UserName,
                    UserPassword = dbUser[0].UserPassword,
                    UserID = dbUser[0].UserID,
                    JwtToken = tokenStr
                };
            }
            return new UserModel()
            {
                UserPassword = "ERROR",
                UserName = "ERROR",
                UserID = 0,
                JwtToken = ""
            };
        }

        // GET api/<Access>/5
        [HttpPost]
        public async Task<ActionResult<UserModel>> PostUser(UserModel user)
        {
            if(await _ControllDataBaseAccess.ExecDbAsync<UserModel>(user, eAccessDb.InsertUser))
            {
                var newDbuser = await _ControllDataBaseAccess.GetDbAsync<List<UserModel>, UserModel>(user, eAccessDb.ReadUser);
                if(newDbuser.Count == 1)
                {
                    return CreatedAtAction(
                        nameof(Get), 
                        new 
                        { 
                            UserName = newDbuser[0].UserName,
                            UserPassword = newDbuser[0].UserPassword
                        }, 
                        newDbuser
                        );
                }
            }
            
            return CreatedAtAction(nameof(Get), new { }, user);
        }
    }
}
